The python script is used to convert .mrc workout files created in the training section of golden cheetah.
This is useful when one want to track and recreate exercises done for example on a indoor bike at a gym were one can see the power data but can' record it. Currently it ONLY works for relative power data.

USAGE:

1) Convert only one file:
	- place the marcTOcsv.py in the folder which contains all of the .mrc workout files
	- execute the script with the name of the .mrc file you want to convert and your current ftp in watts
	- in my case:python3 mrcTOcsv.py "name_of_file.mrc" 8000
	- the .csv that is created can be imported in GC as an activity


2) Convert all files in the current folder
	- place the marcTOcsv.py in the folder which contains all of the .mrc workout files
	- execute the script with the current ftp in watts as an argument
	- in my case:python3 mrcTOcsv.py 8000
	- the .csv's that are created can be imported in GC as an activity
