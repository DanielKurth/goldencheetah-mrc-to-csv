import csv
import sys
import os

output_folder_name = "marcTOcsv output"
mrc_files_names = []
ftp = 0

# -----get filename(s) and ftp from input args-----
if len(sys.argv) == 3:
    # the case if only one file is supposed to be converted
    mrc_files_names = sys.argv[1]
    ftp = int(sys.argv[2])
elif len(sys.argv) == 2: 
    # the case if all files in the direcrtory are supposed to be converted
    ftp = int(sys.argv[1])
    # get all the file names in the current directory
    workout_dir = os.listdir()
    # store all the .mrc file names in the current directory
    mrc_files_names = []
    for dir_elem in workout_dir:
        # check if the file is a .mrc workout file
        if dir_elem[-4:] == ".mrc":
            # build mrc file name list
            mrc_files_names.append(dir_elem)
else:
    print("Arguments are not valid")
    
# create the output folde if it doesn't exit
from pathlib import Path
Path(output_folder_name).mkdir(parents=True, exist_ok=True)

# -----convert the mrc files(s)-----
for mrc_file_name in mrc_files_names:
    # open mrc training file 
    mrc_file = open(mrc_file_name,"r")
    # convert file into list
    f_list = list(mrc_file)
    # get the index were the workout data starts
    data_start = f_list.index('[COURSE DATA]\n')+1
    # get the index were the workout data ends
    data_end =  f_list.index('[END COURSE DATA]\n')-1
    # generate .csv file name string
    csv_file_name = mrc_file_name[:-4]+".csv"
    # create the first line of the csv with all the headers
    with open(output_folder_name+"/"+csv_file_name, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(['secs','cad','hr','km','kph','nm','watts'\
        ,'alt','lon','lat','headwind','slope','temp','interval'\
        ,'lrbalance','lte','rte','lps','rps','smo2','thb','o2hb','hhb'])
        # represent the time in seconds of a current workout while going through it
        time = 0;
        # itterarte over all the data elements in the list
        data_iter = data_start
        while data_iter <= data_end:
            # data_str = ['min' 'pwr']
            data_str = f_list[data_iter].split()
            # convert min into secconds
            sec = float(data_str[0])*60
            # convert perentage and ftp to actual power in watts
            pwr = round(float(data_str[1])*0.01*ftp, 0)
            # write one line for every seccond
            while(time<=sec):
                # all data but time and power is set to zero
                writer.writerow([time,0,0,0,0,0,pwr,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
                # increment time by one seccond
                time = time+1;
            data_iter = data_iter+1


